# mmltk

A small python command line tool to translate MIDI files into MML
(Music Macro Language) and vice versa.


## Installation

To install this tool, clone or copy the project to any location.


## Dependencies

To run the program, the following Python packages must be installed
(developed with the versions indicated in brackets, newer versions may
work as well):

- mido (1.2.9)
- numpy (1.19.4)
- pandas (1.1.4)


## How it is run?

The simplest form of processing is to pass input, and output

    python mmltk.py -i InputFile.mid -o OutputFile.mml

The program help is available with the command

    python mmltk.py -h

